import React from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity, Dimensions, TextInput} from 'react-native';
import {addProductToApi} from './../redux/actions'
import {fetchProductsFromApi} from './../redux/actions'
import { connect } from 'react-redux';





const screen = Dimensions.get("window");



class AddProductModal extends React.Component{
   state = {
       ID: 10,
       Name: "",
       Price: ""
   }
 
   onSubmitForm(){
    if( this.state.Name.length == 0 || this.state.Price.length == 0){
        alert("You must enter a name and a price for the new product.")
        return 
    } else {
        console.log(this.state)
        this.props.addProduct(this.state) 
        this.props.getProducts();
    }
      
  

    }

    
    render(){
      
        return (
          <View>
              <Text style={styles.titleStyle}>Add a product</Text>

              <TextInput placeholder="Add product name" 
                         style={styles.inputStyle}  
                         value={this.state.Name} 
                         onChangeText={(text)=> this.setState({ Name: text})}
                         /> 

              <TextInput placeholder="Add product price" 
                         style={styles.inputStyle} 
                         value={this.state.Price} 
                         onChangeText={(text)=> this.setState({ Price: text})} 
                        
                         /> 

              <TouchableOpacity 
                         style={styles.buttonStyle}
                         onPress={() => this.onSubmitForm()}
                         >
                  <Text style={styles.buttonText}>Add Product</Text>
              </TouchableOpacity>    

              <TouchableOpacity style={styles.buttonBackStyle} onPress={()=> this.props.navigation.goBack()}>
                  <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>  
          </View> 
        )
    }
}
const styles = StyleSheet.create({
     titleStyle: {
       fontSize: 16,
       fontWeight: "bold",
       textAlign: "center",
       marginTop: 40
     },
     inputStyle: {
       height: 40,
       borderBottomColor: "gray",
       marginLeft: 30,
       marginRight: 30,
       marginTop: 20,
       marginBottom: 10,
       borderBottomWidth: 1
     },
     buttonStyle:{
        height: 40,
        flex: 1,
        padding: 20,
        backgroundColor: "red",
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20
    
     },
     buttonText: {
         
         color: "white"
     },
     buttonBackStyle:{
        height: 40,
        flex: 1,
        padding: 20,
        backgroundColor: "gray",
        marginLeft: 30,
        marginRight: 30,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20
    
     }
})

   
function mapStateToProps(state){

    return {
        products: state.products
    }
  }
  
  function mapDispatchToProps(dispatch){
    return {
        addProduct: (data) => dispatch(addProductToApi(data)),
        getProducts: () => dispatch(fetchProductsFromApi())
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddProductModal)