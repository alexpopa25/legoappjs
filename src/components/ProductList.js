import React from 'react';
import {createRef} from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity} from 'react-native';

import { connect } from 'react-redux';
import {fetchProductsFromApi} from './../redux/actions'
import  ProductListItem  from './ProductListItem';
import AddProductModal from './AddProductModal';
import {NavigationScreenProps} from 'react-navigation'






class ProductList extends React.Component{
  
  
    componentDidMount(){
       
        this.props.getProducts()
       
    }
    
 
 
    
    render(){
        const { products, isFetching }  = this.props.products;
       
        console.log('products: ', products.d)
        // Object.keys(products).length
        return (
            <View>
           
                    <FlatList
                     data={products.d}
                     keyExtractor={(item, index) => index.toString()}
                     renderItem={({item, index})=>{
                       return (
                           <ProductListItem item={item} index={index}>
                           </ProductListItem>
                       );
                     }}
                     
                     >
                    
                    </FlatList>   
                
                    
                  
            </View>
               
        
            
        )
    }

}

const styles = StyleSheet.create({
  viewStyle: {
      flex: 1
  }
 
})
function mapStateToProps(state){

  return {
      products: state.products
  }
}

function mapDispatchToProps(dispatch){
  return {
      getProducts: () => dispatch(fetchProductsFromApi())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)