import React from 'react';
import { StyleSheet, Text, View, Image, FlatList} from 'react-native';


export default class ProductListItem extends React.Component{
    
    render(){
        // console.log(this.props.item)
        return (
            <View style={styles.viewStyles}>
              <View style={styles.innerContainer}>
                 <View style={styles.itemDetails}>
                   <Text style={styles.itemTitle}>Product: {this.props.item.ID}</Text>
                   <Text>{this.props.item.Name}</Text>
                   
                 </View> 
              </View>    
              <Text style={styles.itemPrice}>${this.props.item.Price}</Text> 
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    viewStyles: {
       flexDirection: "row",
       justifyContent: "space-between",
       alignItems: "center",
       height: 70,
       paddingLeft: 30,
       paddingRight: 30,
       marginBottom: 10,
       borderBottomColor: 'lightgray',
       borderBottomWidth: 1,
    },
    innerContainer:  {
        flexDirection: "row",
        justifyContent: "space-around"
    },
    itemDetails: {
        flexDirection: "column"
    },
    itemTitle: {
      color: "blue"
    },
    itemPrice: {
        fontWeight: "bold"
    },
  border: {
   
  }
})