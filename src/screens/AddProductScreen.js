/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { Component } from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import ProductList from '../components/ProductList';
import {createStackNavigator} from 'react-navigation';


import Header from '../components/Header'
import AddProductModal from '../components/AddProductModal';

  

export default class AddProductScreen extends Component {
  render() {
    return (
      <View >
        <Header title="Add new product" logo_url="https://s3.amazonaws.com/freebiesupply/thumbs/2x/lego-logo.png" />
        <AddProductModal navigation={this.props.navigation}/>
        
      </View>
   
    );
  }
}

const styles = StyleSheet.create({

});
