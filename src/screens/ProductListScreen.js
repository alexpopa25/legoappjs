/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { Component } from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import ProductList from '../components/ProductList';
import Header from '../components/Header';
import {createStackNavigator} from 'react-navigation';




  

export default class ProductListScreen extends Component {
  render() {
    return (
      <View >
        <Header title="My Products" logo_url="https://s3.amazonaws.com/freebiesupply/thumbs/2x/lego-logo.png" />
        <ProductList />
        <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.btn} onPress={()=> this.props.navigation.navigate('AddProduct')}>
                        <Text style={styles.btnText}>+</Text>
                    </TouchableOpacity> 
                    </View>
      </View>
   
    );
  }
}

const styles = StyleSheet.create({
  btnContainer: {
    position: "absolute",
    width: 70,
    height:70,
    right: 30,
    bottom: 100,
 
},
btn: {
   
    width: 70,
    height:70,
    backgroundColor: "red",
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
   
},
btnText: {
    color: "white",
    fontSize: 25
}
})

