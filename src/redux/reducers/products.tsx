import {FETCHING_PRODUCTS,FETCHING_PRODUCTS_SUCCESS,FETCHING_PRODUCTS_FAILURE, ADDING_PRODUCT, ADDING_PRODUCT_SUCCESS, ADDING_PRODUCT_FAILURE  } from '../constants';

const initialState = {
    products: [{}],
    isFetching: false,
    error: false
}

export default function productsReducer(state = initialState, action: any){
    console.log("after: ", state.products.d)
    switch(action.type){
        case FETCHING_PRODUCTS: 
            return {
                ...state,
                isFetching: true,
                products: [{}]
            }
        case FETCHING_PRODUCTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                products: action.data
            }
        case FETCHING_PRODUCTS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true
            }
        case ADDING_PRODUCT: 
        // console.log('reducer: ', action.data)
        // console.log('reducer: ', ...state.products.d)
        return { 
            ...state,
            products: [...state.products.d, action.data]
           
        }
        
        
            
        // case ADDING_PRODUCT_SUCCESS:
        //     return {
        //         ...state
        //     }
        //     case ADDING_PRODUCT_FAILURE:
        //     return {
        //         ...state
        //     }    

        default: 
            return state;    
    }
}