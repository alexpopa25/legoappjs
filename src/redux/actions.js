import {FETCHING_PRODUCTS,FETCHING_PRODUCTS_SUCCESS,FETCHING_PRODUCTS_FAILURE, ADDING_PRODUCT, ADDING_PRODUCT_SUCCESS, ADDING_PRODUCT_FAILURE  } from './constants';


export function fetchProductsFromApi(){
    return (dispatch) => {
        dispatch(getProducts())
        fetch('https://services.odata.org/V2/(S(1pvfcxajki3ouml43k5wnex3))/OData/OData.svc/Products', {
            headers: {
                "Accept": "application/json"
            }
        })
              .then((response)=>{
                  return response.json()
              })
              .then((response)=>{
                dispatch(getProductsSuccess(response))
              })
              .catch(err => dispatch(getProductsFailure(err)))
    }
}

function getProducts(){
  return {
      type: FETCHING_PRODUCTS
  }
}

function getProductsSuccess(data){
    // console.log(data)
    return {
        type: FETCHING_PRODUCTS_SUCCESS,
        data
    }
  }

  function getProductsFailure(err){
    return {
        type: FETCHING_PRODUCTS_FAILURE
    
    }
  }

  export function addProductToApi(data){
    return (dispatch) => {
        dispatch(addProduct(data))
        fetch('https://services.odata.org/V2/(S(1pvfcxajki3ouml43k5wnex3))/OData/OData.svc/Products', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        })
              .then((response)=>{
                  return response
                  
              })
              .then((response)=>{
                // dispatch(addProductSuccess(response))
                console.log(" from response ",response)
                
              })
              .catch((err)=>{return console.log(err)})
     }
    // return (dispatch)=>{console.log("actions", data)}
   
}


function addProduct(data){
  return {
      type: ADDING_PRODUCT,
      data
  }
}

// function addProductSuccess(data){
//     // console.log(data)
//     return {
//         type: FETCHING_PRODUCTS_SUCCESS,
//         data
//     }
//   }

//   function addProductFailure(err){
//     return {
//         type: FETCHING_PRODUCTS_FAILURE
    
//     }
//   }