/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { Component } from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import ProductList from './components/ProductList';
import {createStackNavigator} from 'react-navigation';


import Header from './components/Header'
import ProductListScreen from './screens/ProductListScreen';
import AddProductScreen from './screens/AddProductScreen';

 
export default class App extends Component {
  render() {
    return (
     <AppStackNavigator />
    );
  }
}

const AppStackNavigator = createStackNavigator({
  ProductList: ProductListScreen,
  AddProduct: AddProductScreen
},
{
  headerMode: "none",
  navigationOptions: {
    headerVisible: false,
  }
}
)


